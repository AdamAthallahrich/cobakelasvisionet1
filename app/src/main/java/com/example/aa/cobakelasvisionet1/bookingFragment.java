package com.example.aa.cobakelasvisionet1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link bookingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link bookingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class bookingFragment extends Fragment {

    public bookingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booking, container, false);
    }

}
